//
//  RBLLightViewController.m
//  BLE RGB
//
//  Created by redbear on 14-2-20.
//  Copyright (c) 2014年 redbear. All rights reserved.
//

#import "RBLLightViewController.h"

@interface RBLLightViewController ()

@end

@implementation RBLLightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _colorPicker = [[RSColorPickerView alloc] initWithFrame:CGRectMake(30.0, 80.0, 280.0, 260.0)];
    [_colorPicker setCropToCircle:YES]; // Defaults to YES (and you can set BG color)
    [_colorPicker setDelegate:self];
    [self.view addSubview:_colorPicker];
    
    _colorPatch = [[UIView alloc] initWithFrame:CGRectMake(100, 355.0, 120, 40.0)];
	[self.view addSubview:_colorPatch];
    
    int labelY = 400;
    
    // Buttons for testing
    UIButton *selectRed = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    selectRed.frame = CGRectMake(20.0, labelY, 50.0, 30.0);
    [selectRed setTitle:@"Red" forState:UIControlStateNormal];
    [selectRed addTarget:self action:@selector(selectRed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectRed];
    
    UIButton *selectGreen = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    selectGreen.frame = CGRectMake(CGRectGetMaxX(selectRed.frame) + 10, labelY, 50.0, 30.0);
    [selectGreen setTitle:@"Green" forState:UIControlStateNormal];
    [selectGreen addTarget:self action:@selector(selectGreen:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectGreen];
    
    UIButton *selectBlue = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    selectBlue.frame = CGRectMake(CGRectGetMaxX(selectGreen.frame) + 10, labelY, 50.0, 30.0);
    [selectBlue setTitle:@"Blue" forState:UIControlStateNormal];
    [selectBlue addTarget:self action:@selector(selectBlue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectBlue];
    
    UIButton *selectWhite = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    selectWhite.frame = CGRectMake(CGRectGetMaxX(selectBlue.frame) + 10, labelY, 50.0, 30.0);
    [selectWhite setTitle:@"White" forState:UIControlStateNormal];
    [selectWhite addTarget:self action:@selector(selectWhite:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectWhite];
    
    UIButton *selectBlack = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    selectBlack.frame = CGRectMake(CGRectGetMaxX(selectWhite.frame) + 10, labelY, 50.0, 30.0);
    [selectBlack setTitle:@"Off" forState:UIControlStateNormal];
    [selectBlack addTarget:self action:@selector(selectOff:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectBlack];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 440, 320, 30.0)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Arial" size:15.0f];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
	_colorPatch.backgroundColor = [cp selectionColor];
    //    _brightnessSlider.value = [cp brightness];
    //    _opacitySlider.value = [cp opacity];
    
    label.text = [NSString stringWithFormat:@"R : %d\tG : %d\tB : %d", [cp vRed], [cp vGreen], [cp vBlue]];
    
    uint8_t command[] = {'O', 0, 0x00,0x00,0x00};
    
    command[2] = [cp vRed];
    command[3] = [cp vGreen];
    command[4] = [cp vBlue];
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:5];
    NSLog(@"Sent %@", nsData);
    
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
    
    //[NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(party) userInfo:nil repeats:YES];
}

- (void) party {
    uint8_t command[] = {'O', 0, 0x00,0x00,0x00};
    
    command[1] = arc4random_uniform(24);
    command[2] = arc4random_uniform(255);
    command[3] = arc4random_uniform(255);
    command[4] = arc4random_uniform(255);
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:5];
    NSLog(@"Sent %@", nsData);
    
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

#pragma mark - User action

- (void)selectRed:(id)sender {
    [_colorPicker setSelectionColor:[UIColor redColor]];
}
- (void)selectGreen:(id)sender {
    [_colorPicker setSelectionColor:[UIColor greenColor]];
}
- (void)selectBlue:(id)sender {
    [_colorPicker setSelectionColor:[UIColor blueColor]];
}
- (void)selectBlack:(id)sender {
    [_colorPicker setSelectionColor:[UIColor blackColor]];
}
- (void)selectWhite:(id)sender {
    [_colorPicker setSelectionColor:[UIColor whiteColor]];
}
- (void)selectPurple:(id)sender {
    [_colorPicker setSelectionColor:[UIColor purpleColor]];
}
- (void)selectCyan:(id)sender {
    [_colorPicker setSelectionColor:[UIColor cyanColor]];
}

- (void)selectOff:(id)sender {
    uint8_t command[] = {'A', 0x00, 0x00, 0x00};
    
    NSData *nsData = [[NSData alloc] initWithBytes:command length:4];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

- (void)circleSwitchAction:(UISwitch *)s {
	_colorPicker.cropToCircle = s.isOn;
}

- (IBAction)speedChange:(id)sender {
    UISlider *speedy = (UISlider*)sender;
    //Using char for signed byte-size integer
    char speed = (char)speedy.value;
    char command[] = {'R', speed};
    
    self.speedometer.text = [[NSString alloc] initWithFormat:@"%d", speed];
    NSLog(@"R %d", speed);
    NSData *nsData = [[NSData alloc] initWithBytes:command length:2];
    [self.vc.peripheral writeValue:nsData forCharacteristic:self.vc.txCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

@end
